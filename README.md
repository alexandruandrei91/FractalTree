# Fractal Tree

  A simple C++ implementation of the Pythagoras tree. Used as an experiment to test a few functional programming concepts in C++.

**To Do:**
 * Separate logic from rendering.
 * Add some comments.